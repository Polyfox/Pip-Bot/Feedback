package eu.polyfox.pipbot.feedback;

import eu.polyfox.pipbot.plugin.Plugin;
import lombok.extern.slf4j.Slf4j;

/**
 * Plugin's main class.
 * @author traxam
 */
@Slf4j
public class FeedbackPlugin extends Plugin {
    @Override
    protected void enable() {
        log.info("Enabled feedback.");
    }

    @Override
    protected void disable() {
        log.info("Disabled feedback.");
    }
}
